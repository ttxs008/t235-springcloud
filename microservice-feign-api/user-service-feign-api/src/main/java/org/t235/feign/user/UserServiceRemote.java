package org.t235.feign.user;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.t235.commons.model.AjaxResult;

/**
 * 编写需要暴露的微服务的控制器接口
 */
@FeignClient(name = "user-service", /*fallback = UserServiceRemoteFallback.class*/ fallbackFactory = UserServiceRemoteFallbackFactory.class)
public interface UserServiceRemote {
    @GetMapping("/findById/{userId}")
    AjaxResult findById(@PathVariable("userId") Long userId);

    @GetMapping
    AjaxResult findAll();

    @GetMapping("/userinfo")
    AjaxResult userinfo();
}
