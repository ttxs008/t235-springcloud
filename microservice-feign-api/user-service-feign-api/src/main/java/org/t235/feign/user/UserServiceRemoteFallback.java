package org.t235.feign.user;

import org.springframework.stereotype.Component;
import org.t235.commons.model.AjaxResult;

import java.util.ArrayList;

@Component
public class UserServiceRemoteFallback implements UserServiceRemote{
    @Override
    public AjaxResult findById(Long userId) {
        return AjaxResult.error("用户微服务findById（" + userId + "）正忙", new ArrayList<>());
    }

    @Override
    public AjaxResult findAll() {
        return AjaxResult.error("用户微服务findAll（）正忙", new ArrayList<>());
    }

    @Override
    public AjaxResult userinfo() {
        return AjaxResult.error("用户微服务userinfo（）正忙", new ArrayList<>());
    }
}
