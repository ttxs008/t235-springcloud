package org.t235.feign.order;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.t235.commons.model.AjaxResult;

@Component
@Slf4j
public class OrderServiceRemoteFallbackFactory implements FallbackFactory<OrderServiceRemote> {
    @Override
    public OrderServiceRemote create(Throwable throwable) {
        log.error("OrderService异常", throwable);
        return new OrderServiceRemoteFallback();
    }
}
