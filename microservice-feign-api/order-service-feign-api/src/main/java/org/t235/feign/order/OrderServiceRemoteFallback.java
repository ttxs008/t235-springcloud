package org.t235.feign.order;

import org.springframework.stereotype.Component;
import org.t235.commons.model.AjaxResult;

import java.util.ArrayList;

@Component
public class OrderServiceRemoteFallback implements OrderServiceRemote {
    @Override
    public AjaxResult findAll() {
        return AjaxResult.error("订单微服务findAll（）正忙", new ArrayList<>());
    }

    @Override
    public AjaxResult findByUserId(Long userId) {
        return AjaxResult.error("订单微服务findByUserId（" + userId + "）正忙", new ArrayList<>());
    }
}
