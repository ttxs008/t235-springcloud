package org.t235.feign.order;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.t235.commons.model.AjaxResult;

import javax.sound.midi.SoundbankResource;

/**
 * 编写需要暴露的微服务的控制器接口
 */
@FeignClient(name = "order-service", /*fallback = OrderServiceRemoteFallback.class,*/
        fallbackFactory = OrderServiceRemoteFallbackFactory.class)
public interface OrderServiceRemote {

    @GetMapping
    AjaxResult findAll();

    @GetMapping("/findByUserId")
    AjaxResult findByUserId(@RequestParam("userId") Long userId);
}
