package org.t235.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Long id;
    private String name;
    private List<Order> orders;

    public User(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
