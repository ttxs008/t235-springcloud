package org.t235.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AjaxResult {
    public static final int SUCCESS = 200;
    private int code;
    private String msg;
    private Object data;

    public static AjaxResult success(Object data) {
        return new AjaxResult(SUCCESS, "success", data);
    }
    public static AjaxResult success(String msg, Object data) {
        return new AjaxResult(SUCCESS, msg, data);
    }
    public static AjaxResult error(String msg) {
        return new AjaxResult(500, msg, null);
    }

    public static AjaxResult error(String msg, Object data) {
        return new AjaxResult(500, msg, data);
    }
}
