package org.t235.server;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZuulConfig {
    @Bean
    public IpFilter ipFilter() {
        return new IpFilter();
    }
}
