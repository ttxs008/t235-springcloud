package org.t235.order.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.t235.commons.model.AjaxResult;
import org.t235.commons.model.Order;
import org.t235.order.service.OrderService;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class OrderController {
    @Resource
    private OrderService orderService;
    @Value("${server.port}")
    private String port;
    @Value("${spring.application.name}")
    private String serviceName;

    @GetMapping
    public AjaxResult findAll() {
        if (log.isDebugEnabled()) {
            log.debug("访问"+serviceName+"服务器的端口号是:{}", port);
        }
        return AjaxResult.success(orderService.findAll());
    }

    @GetMapping("/findByUserId")
    public AjaxResult findByUserId(Long userId) {
        List<Order> orders = orderService.findAll().stream().filter(o -> o.getUserId().equals(userId)).collect(Collectors.toList());
        return AjaxResult.success(orders);
    }
}
