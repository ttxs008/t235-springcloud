package org.t235.order.service;

import org.springframework.stereotype.Service;
import org.t235.commons.model.Order;
import org.t235.commons.model.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {
    private static List<Order> list = new ArrayList<>();
    static {
        list.add(new Order(1L,1L, 222.0));
        list.add(new Order(2L,1L, 333.0));
        list.add(new Order(3L,2L, 666.0));
    }
    public List<Order> findAll() {
        return list;
    }
}
