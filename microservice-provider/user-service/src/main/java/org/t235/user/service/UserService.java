package org.t235.user.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;
import org.t235.commons.model.User;
import org.t235.dubbo.user.UserServiceDubboApi;

import java.util.ArrayList;
import java.util.List;

@Service
@DubboService
public class UserService implements UserServiceDubboApi {
    private static List<User> list = new ArrayList<>();
    static {
        list.add(new User(1L,"张三"));
        list.add(new User(2L,"李四"));
    }
    public List<User> findAll() {
        return list;
    }

    @SentinelResource(value = "helloDubbo", blockHandler = "blockHello")
    @Override
    public String hello(String name) {
        return "HELLO DUBBO " + name.toUpperCase();
    }

    public String blockHello(String name, BlockException ex) {
        return "UserService.hello:" + name + "被限流了" + ex;
    }
}
