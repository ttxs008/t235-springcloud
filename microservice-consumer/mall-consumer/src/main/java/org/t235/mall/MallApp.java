package org.t235.mall;

import com.netflix.loadbalancer.RandomRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.t235.commons.model.AjaxResult;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication(scanBasePackages = "org.t235")
@EnableDiscoveryClient
@RestController
@EnableFeignClients(basePackages = "org.t235.feign")
public class MallApp {
    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public AjaxResult hello() {
        AjaxResult userResult = restTemplate.getForObject("http://user-service", AjaxResult.class);
        AjaxResult orderResult = restTemplate.getForObject("http://order-service", AjaxResult.class);
        Map<String, Object> result = new HashMap<>();
        result.put("users", userResult.getData());
        result.put("orders", orderResult.getData());
        return AjaxResult.success(result);
    }

    /*@GetMapping
    public AjaxResult hello() {
        AjaxResult userResult = restTemplate.getForObject(userServiceHost, AjaxResult.class);
        AjaxResult orderResult = restTemplate.getForObject(orderServiceHost, AjaxResult.class);
        Map<String, Object> result = new HashMap<>();
        result.put("users", userResult.getData());
        result.put("orders", orderResult.getData());
        return AjaxResult.success(result);
    }*/

    public static void main(String[] args) {
        SpringApplication.run(MallApp.class, args);
    }
}
