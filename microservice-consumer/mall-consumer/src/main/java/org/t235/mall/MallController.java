package org.t235.mall;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.t235.commons.model.AjaxResult;
import org.t235.dubbo.user.UserServiceDubboApi;
import org.t235.feign.order.OrderServiceRemote;
import org.t235.feign.user.UserServiceRemote;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/mall")
public class MallController {
    @Resource
    private UserServiceRemote userServiceRemote;
    @Resource
    private OrderServiceRemote orderServiceRemote;

    @DubboReference(timeout = 1000, mock = "org.t235.dubbo.user.UserServiceDubboApiMock")
    private UserServiceDubboApi userServiceDubboApi;

    @GetMapping("/userinfo")
    public AjaxResult userinfo() {
        return userServiceRemote.userinfo();
    }


    @GetMapping("/helloDubbo")
    public AjaxResult helloDubbo(String name) {
        return AjaxResult.success(userServiceDubboApi.hello(name));
    }

    @GetMapping("/hello")
    public AjaxResult hello(Long userId) {
        AjaxResult userResult = userServiceRemote.findById(userId);
        AjaxResult orderResult = orderServiceRemote.findAll();
        Map<String, Object> result = new HashMap<>();
        result.put("user", userResult.getData());
        result.put("orders", orderResult.getData());
        String msg = userResult.getMsg() + "," + orderResult.getMsg();

        return AjaxResult.success(msg, result);
    }

    @GetMapping("/findAllUser")
    public AjaxResult findAllUser() {
        return userServiceRemote.findAll();
    }

    @GetMapping("/findAllOrder")
    public AjaxResult findAllOrder() {
        return orderServiceRemote.findAll();
    }
}
