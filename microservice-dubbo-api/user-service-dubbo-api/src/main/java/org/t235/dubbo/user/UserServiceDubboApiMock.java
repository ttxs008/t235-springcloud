package org.t235.dubbo.user;

public class UserServiceDubboApiMock implements UserServiceDubboApi {
    @Override
    public String hello(String name) {
        return "UserServiceDubboApi.hello:" + name + "服务正忙，被熔断了";
    }
}
